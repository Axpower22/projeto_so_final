Trabalho SO

*O programa
*A solu��o
*Como executar o programa 
*Resultados alcan�ados

1. O programa
	O programa � uma solu��o do problema apresentado pelo projeto final de Sistemas Operacionais. O c�digo tem o objetivo de realizar 
	at� 100 tranfer�ncias simult�neas. Uma falha de execu��o ocorre gra�as ao problema de concorr�ncia.

2. A solu��o
	Inicialmente para a cria��o do programa foram utilizadas duas classes, uma para estrututurar o conte�do de cada conta (saldo) e uma para estruturar o conte�do das threads.
	No m�todo construtor foi necess�rio a cria��o de um objeto para guardar a identifica��o das Threads, um para guardar o saldo da conta de sa�da, um para o saldo da conta de entrada,
	uma para guardar o estado da trava, uma para guardar o valor de tranfer�ncia, e uma para guardar a opera��o (tranferir da conta 1 para a 2 ou da 2 para 1)	
	o programa sempre sorteia o valor de cada tranfer�ncia e qual a opera��o que ser� realizada.
	A solu��o usada para o problema foi o uso de sem�foro. Ele � respons�vel pelo controle das Threads: trav�-las ou liber�-las (acquire, release).
	

3. Como executar
	Ao executar em Windowns, basta o programa estar em modo execut�vel. O usu�rio dever� abrir o programa.
	Ao executar em Linux (Ubuntu), o usu�rio deve abrir o terminal (cmd), abrir o diret�rio em que o arquivo foi baixado (cd "Pasta do arquivo") 
	e em seguida executar (python "nome do programa.py"). O python deve estar atualizado em sua ultima vers�o.

4. Resultados
	Foram criadas as 100 threads (Transfer�ncias) proposta. A solu��o de trava funcionou para controlar e execu��o das threads. Com isso
	cada uma n�o impediria a execu��o da outra, resolvendo a assim o problema de concorr�ncia no trabalho. O programa faz um "print" para tornar vis�vel cada 
	opera��o realizada durante a execu��o.
