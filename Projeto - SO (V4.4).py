#!/usr/bin/python
# -*- coding: latin-1 -*-
import threading
import random
import time

class Conta():  # Classe das contas
    def __init__(self, saldo):
        self.saldo = saldo

class MinhaThread(threading.Thread, Conta):
    def __init__(self, meuId, saldo_from, saldo_to, trava, valor, op):
        self.meuId = meuId  # Identificador das Threads (Tranferencias)
        self.saldo_conta_from = saldo_from  # Saldo da conta de saida
        self.saldo_conta_to = saldo_to  # Saldo da conta de entrada
        self.trava = trava  # Variavel de trava
        self.valor = valor  # Valor das tranferencias
        self.operacao = op  # Decisao de qual conta o dinheiro sai
        threading.Thread.__init__(self)

    def run(self):    # Parte responsavel pelo que a thread vai executar
        with self.trava:   # Parte responsavel por travar e liberar as threads (acquire, release)
            print('\nSaldo da conta 1 antes da tranferencia: R$', conta1.saldo)
            print('Saldo da conta 2 antes da transferencia: R$', conta2.saldo)

            if self.saldo_conta_from >= self.valor and self.operacao == 1:  # Tranferencia da conta 1 para a 2
                conta1.saldo -= self.valor
                conta2.saldo += self.valor
                print('\nTranferencia', self.meuId, 'de R$', self.valor, 'da conta 1 para a conta 2 concluida com sucesso!')
                print('\nSaldo da conta 1 apos tranferencia: R$', conta1.saldo)
                print('\nSaldo da conta 2 apos tranferencia: R$', conta2.saldo)
            elif self.saldo_conta_from < self.valor and self.operacao == 1:  # Tratamento de excessao
                print('\nNao foi possivel fazer a tranferencia',self.meuId, 'de R$',self.valor, 'da conta 1 para a conta 2.')

            elif self.saldo_conta_from >= self.valor and self.operacao == 2:  # Tranferencia da conta 2 para a 1
                conta2.saldo -= self.valor
                conta1.saldo += self.valor
                print('\nTranferencia' ,self.meuId, 'de R$' ,self.valor, 'da conta 2 para a conta 1 concluida com sucesso!')
                print('\nSaldo da conta 1 apos tranferencia: R$',conta1.saldo)
                print('\nSaldo da conta 2 apos tranferencia: R$',conta2.saldo)
            elif self.saldo_conta_from < self.valor and self.operacao == 2:  # Tratamento de excessao
                print('\nNao foi possivel fazer a tranferencia',self.meuId, 'de R$',self.valor, 'da conta 2 para a conta 1.\n')

if __name__ == '__main__':
    trava = threading.Lock()  # Trava resposavel pelo controle das Threads
    threads = []  # Lista para guardar as threads
    conta1 = Conta(100)  # Definicao do valor inicial da conta 1
    conta2 = Conta(100)  # Definicao do valor inicial da conta 2

    for id in range(1, 101):  # Definicao de quantas tranferencias serao executadas
        val = random.randint(1, 100)  # Sorteio de quanto dinheiro ser tranferido
        op = random.randint(1, 2) # Sorteio de decisao de qual conta o dinheiro vai sair

        if op == 1:
            thread = MinhaThread(id, conta1.saldo, conta2.saldo, trava, val, op)  # Isso cria a thread
            thread.start()  # Isso executa ela
            threads.append(thread)  # Isso joga a thread dentro de uma lista

            for thread in threads:
                thread.join()  # Isso espera cada thread da lista finalizar


        elif op == 2:
            thread = MinhaThread(id, conta2.saldo, conta1.saldo, trava, val, op)  # Isso cria a thread
            thread.start()  # Isso executa ela
            threads.append(thread)  # Isso joga a thread dentro de uma lista

            for thread in threads:
                thread.join()  #Isso espera cada thread da lista finalizar

    time.sleep(3600)
